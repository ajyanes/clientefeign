package com.curso.feign.demo.service;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import com.curso.feign.demo.model.entity.ResponseDto;
import com.curso.feign.demo.model.entity.UserDto;

@FeignClient(name = "user-service", url = "localhost:8080")
public interface FeignService {
    
    @GetMapping("/user/listar")
    public List<UserDto> findAll();
}
