/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.curso.feign.demo.controller;

import com.curso.feign.demo.model.entity.ResponseDto;
import com.curso.feign.demo.model.entity.UserDto;
import com.curso.feign.demo.service.FeignService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private FeignService feignService;

    @GetMapping("/list")
    public List<UserDto> findAll() {

        return feignService.findAll();
    }
}
