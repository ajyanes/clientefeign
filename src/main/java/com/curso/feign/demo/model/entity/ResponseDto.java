/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.curso.feign.demo.model.entity;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ResponseDto<DTO> {

    DTO data;
    String message;
    String status;

    public ResponseDto(String message, String status) {
        super();
        this.message = message;
        this.status = status;
    }

    public ResponseDto(DTO data, String message, String status) {
        super();
        this.data = data;
        this.message = message;
        this.status = status;
    }
}
